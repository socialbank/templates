# Templates

## Public Generamba templates

To include those templates in your project, install [Generamba](https://github.com/strongself/Generamba) and add the following lines to your _Rambafile_:

```
### Templates
catalogs:
- 'https://gitlab.com/socialbank/templates'
templates:
- {name: ios_viper}
- {name: ios_viper_model}
- {name: ios_viper_addon}
````

To install the templates run:

```
generamba template install
```

To use one of the templates run:

```
generamba gen ModuleName templateName
```

That's all. Enjoy it (: